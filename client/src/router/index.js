import Vue from 'vue'
import Router from 'vue-router'

import ReceiptIndex from '@/components/receipts/Index'
import ReceiptCreateOrUpdate from '@/components/receipts/CreateOrUpdate'

Vue.use(Router)

const routes = [
  { path: '/receipts/', name: 'ReceiptIndex', component: ReceiptIndex },
  { path: '/receipts/add', name: 'ReceiptCreate', component: ReceiptCreateOrUpdate },
  { path: '/receipts/:id/edit', name: 'ReceiptEdit', component: ReceiptCreateOrUpdate },
]

export default new Router({
  routes
})
