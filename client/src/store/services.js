import Axios from 'axios'
import receiptService from '../services/ReceiptService'

let apiUrl = 'http://localhost:61381/api/Receipt'

// Axios Configuration
Axios.defaults.headers.common.Accept = 'application/json'

export default {
    receiptService: new receiptService(Axios, apiUrl)
}