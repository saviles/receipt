# README #

API Rest project and client-side project for receipt management

## Frameworks and Libraries

Backend

- [Net Core]
- [Entity Framework Core]
- [Swashbuckle AspNetCore(Swagger)]
- [AutoMapper]
- [Docker]

Frontend

- [Vue js]
- [Element UI]
- [Axios]

Data Base

- [SQL Server]