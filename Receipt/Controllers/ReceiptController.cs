﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Receipt.Models;
using Receipt.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Receipt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReceiptController : ControllerBase
    {
        private readonly IReceiptService _receiptService;
        private readonly IMapper _mapper;

        public ReceiptController(IReceiptService receiptService, IMapper mapper)
        {
            _receiptService = receiptService;
            _mapper = mapper;
        }

        // GET: api/Receipt
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(IEnumerable<ReceiptDTO>), (int)HttpStatusCode.OK)]
        public ActionResult<IEnumerable<ReceiptDTO>> Get()
        {
            var receipts = _receiptService.GetReceipts();
            if (receipts != null && receipts.Count() > 0) 
            {
                var receiptsDTO = _mapper.Map<List<ReceiptDTO>>(receipts);
                return Ok(receiptsDTO);
            }
            return NotFound();
        }

        // GET: api/Receipt/5
        [HttpGet("{id}", Name = "Get")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ReceiptDTO), (int)HttpStatusCode.OK)]
        public ActionResult<ReceiptDTO> Get(int id)
        {
            if (id <= 0)
                return BadRequest();
            var receipt = _receiptService.GetReceiptByID(id);
            if (receipt != null) 
            {
                var receiptDTO = _mapper.Map<ReceiptDTO>(receipt);
                return Ok(receiptDTO);
            }
            return NotFound();
        }

        // POST: api/Receipt
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ReceiptDTO), (int)HttpStatusCode.OK)]
        public ActionResult<ReceiptDTO> Post([FromBody] ReceiptDTO receiptDTO)
        {
            if (receiptDTO == null)
                return BadRequest();
            var receipt = _mapper.Map<ReceiptDTO, Receipt.Entities.Receipt>(receiptDTO);
            _receiptService.InsertReceipt(receipt);
            if (receipt.Id > 0) 
            {
                var response = _mapper.Map<ReceiptDTO>(receipt);
                return Ok(response);
            }
            return NotFound();
        }

        // PUT: api/Receipt/5
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Put(int id, [FromBody] ReceiptDTO receiptDTO)
        {
            if (receiptDTO == null || id <= 0)
                return BadRequest();
            var receipt = _receiptService.GetReceiptByID(id);
            if (receipt == null)
                return NotFound();
            receipt.SupplierId = receiptDTO.SupplierId;
            receipt.Amount = receiptDTO.Amount;
            receipt.Commentary = receiptDTO.Commentary;
            receipt.Date = receiptDTO.Date;
            receipt.CoinId = receiptDTO.CoinId;
            _receiptService.UpdateReceipt(receipt);
            return Ok();
        }

        // DELETE: api/Receipt/5
        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ReceiptDTO), (int)HttpStatusCode.OK)]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
                return BadRequest();
            _receiptService.DeleteReceipt(id);
            return Ok();
        }
    }
}
