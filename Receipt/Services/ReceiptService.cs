﻿using System.Collections.Generic;
using System.Transactions;
using Receipt.Repository;

namespace Receipt.Services
{
    public class ReceiptService : IReceiptService
    {
        private readonly IReceiptRepository _receiptRepository;

        public ReceiptService(IReceiptRepository receiptRepository)
        {
            _receiptRepository = receiptRepository;
        }

        public IEnumerable<Receipt.Entities.Receipt> GetReceipts()
        {
            return _receiptRepository.GetReceipts();
        }

        public Receipt.Entities.Receipt GetReceiptByID(int receiptId)
        {
            return _receiptRepository.GetReceiptByID(receiptId);
        }

        public void InsertReceipt(Receipt.Entities.Receipt receipt)
        {
            using (var scope = new TransactionScope())
            {
                _receiptRepository.InsertReceipt(receipt);
                scope.Complete();
            }
        }

        public void UpdateReceipt(Receipt.Entities.Receipt receipt)
        {
            using (var scope = new TransactionScope())
            {
                _receiptRepository.UpdateReceipt(receipt);
                scope.Complete();
            }
        }

        public void DeleteReceipt(int receiptId)
        {
            _receiptRepository.DeleteReceipt(receiptId);
        }
    }
}
