﻿using System.Collections.Generic;

namespace Receipt.Services
{
    public interface IReceiptService
    {
        IEnumerable<Receipt.Entities.Receipt> GetReceipts();
        Receipt.Entities.Receipt GetReceiptByID(int receiptId);
        void InsertReceipt(Receipt.Entities.Receipt receipt);
        void DeleteReceipt(int receiptId);
        void UpdateReceipt(Receipt.Entities.Receipt receipt);
    }
}
