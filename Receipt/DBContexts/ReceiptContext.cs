﻿using Microsoft.EntityFrameworkCore;
using Receipt.Entities;

namespace Receipt.DBContexts
{
    public class ReceiptContext : DbContext
    {
        public ReceiptContext(DbContextOptions<ReceiptContext> options) : base(options)
        {
        }

        public DbSet<Receipt.Entities.Receipt> Receipts { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Coin> Coins { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Receipt.Entities.Receipt>()
                .HasOne(e => e.Supplier)
                .WithOne(e => e.Receipt)
                .HasForeignKey<Receipt.Entities.Receipt>(e => e.SupplierId);

            modelBuilder.Entity<Receipt.Entities.Receipt>()
                .HasOne(e => e.Coin)
                .WithOne(e => e.Receipt)
                .HasForeignKey<Receipt.Entities.Receipt>(e => e.CoinId);

            //Initial data is added for the Coins table
            modelBuilder.Entity<Coin>().HasData(
                new Coin
                {
                    Id = 1,
                    Name = "Euro"
                },
                new Coin
                {
                    Id = 2,
                    Name = "Mexican peso"
                },
                new Coin
                {
                    Id = 3,
                    Name = "American dollar"
                }
            );

            //Initial data is added for the Suppliers table
            modelBuilder.Entity<Supplier>().HasData(
                new Supplier
                {
                    Id = 1,
                    Name = "Nikola Tesla"
                },
                new Supplier
                {
                    Id = 2,
                    Name = "Marie Curie"
                },
                new Supplier
                {
                    Id = 3,
                    Name = "Gregor Mendel"
                }
            );
        }
    }
}
