﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Receipt.DBContexts;

namespace Receipt.Repository
{
    public class ReceiptRepository : IReceiptRepository
    {
        private readonly ReceiptContext _dbContext;

        public ReceiptRepository(ReceiptContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void DeleteReceipt(int receiptId)
        {
            var product = _dbContext.Receipts.Find(receiptId);
            _dbContext.Receipts.Remove(product);
            Save();
        }

        public Receipt.Entities.Receipt GetReceiptByID(int receiptId)
        {
            return _dbContext.Receipts
                            .Include(r => r.Supplier)
                            .Include(r => r.Coin)
                            .FirstOrDefault(r => r.Id == receiptId);
        }

        public IEnumerable<Receipt.Entities.Receipt> GetReceipts()
        {
            return _dbContext.Receipts
                            .Include(r => r.Supplier)
                            .Include(r => r.Coin)
                            .ToList();
        }

        public void InsertReceipt(Receipt.Entities.Receipt receipt)
        {
            _dbContext.Add(receipt);
            Save();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void UpdateReceipt(Receipt.Entities.Receipt receipt)
        {
            _dbContext.Entry(receipt).State = EntityState.Modified;
            Save();
        }
    }
}
