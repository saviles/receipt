﻿namespace Receipt.Models
{
    /// <summary>
    /// Class with properties for Coin
    /// </summary>
    public class CoinDTO
    {
        /// <summary>
        /// Supplier identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the Supplier
        /// </summary>
        public string Name { get; set; }
    }
}
