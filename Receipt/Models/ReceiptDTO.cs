﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Receipt.Models
{
    /// <summary>
    /// Class with properties for Receipt
    /// </summary>
    public class ReceiptDTO
    {
        /// <summary>
        /// Receipt identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Amount of the Receipt
        /// </summary>
        [Required]
        public decimal Amount { get; set; }

        /// <summary>
        /// Date of the Receipt
        /// </summary>
        [Required]
        public DateTime Date { get; set; }

        /// <summary>
        /// Commentary of the Receipt
        /// </summary>
        public string Commentary { get; set; }

        /// <summary>
        /// Relationship with the Supplier
        /// </summary>
        [Required]
        public int SupplierId { get; set; }

        /// <summary>
        /// Relationship with the Supplier
        /// </summary>
        public SupplierDTO Supplier { get; set; }

        /// <summary>
        /// Relationship with the Coin
        /// </summary>
        [Required]
        public int CoinId { get; set; }

        /// <summary>
        /// Relationship with the Coin
        /// </summary>
        public CoinDTO Coin { get; set; }
    }
}
