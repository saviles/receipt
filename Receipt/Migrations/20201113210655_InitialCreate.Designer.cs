﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Receipt.DBContexts;

namespace Receipt.Migrations
{
    [DbContext(typeof(ReceiptContext))]
    [Migration("20201113210655_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Receipt.Models.Coin", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Coins");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Euro"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Mexican peso"
                        },
                        new
                        {
                            Id = 3,
                            Name = "American dollar"
                        });
                });

            modelBuilder.Entity("Receipt.Models.Receipt", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<decimal>("Amount");

                    b.Property<int>("CoinId");

                    b.Property<string>("Commentary");

                    b.Property<DateTime>("Date");

                    b.Property<int>("SupplierId");

                    b.HasKey("Id");

                    b.HasIndex("CoinId")
                        .IsUnique();

                    b.HasIndex("SupplierId")
                        .IsUnique();

                    b.ToTable("Receipts");
                });

            modelBuilder.Entity("Receipt.Models.Supplier", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Suppliers");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Nikola Tesla"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Marie Curie"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Gregor Mendel"
                        });
                });

            modelBuilder.Entity("Receipt.Models.Receipt", b =>
                {
                    b.HasOne("Receipt.Models.Coin", "Coin")
                        .WithOne("Receipt")
                        .HasForeignKey("Receipt.Models.Receipt", "CoinId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Receipt.Models.Supplier", "Supplier")
                        .WithOne("Receipt")
                        .HasForeignKey("Receipt.Models.Receipt", "SupplierId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
