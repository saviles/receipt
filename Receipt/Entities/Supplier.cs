﻿namespace Receipt.Entities
{
    /// <summary>
    /// Class with properties for Supplier
    /// </summary>
    public class Supplier
    {
        /// <summary>
        /// Supplier identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the Supplier
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Relationship with the Receipt
        /// </summary>
        public Receipt Receipt { get; set; }
    }
}
