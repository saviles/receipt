﻿using System;

namespace Receipt.Entities
{
    /// <summary>
    /// Class with properties for Receipt
    /// </summary>
    public class Receipt
    {
        /// <summary>
        /// Receipt identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Amount of the Receipt
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Date of the Receipt
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Commentary of the Receipt
        /// </summary>
        public string Commentary { get; set; }

        /// <summary>
        /// Relationship with the Supplier
        /// </summary>
        public int SupplierId { get; set; }

        /// <summary>
        /// Relationship with the Supplier
        /// </summary>
        public Supplier Supplier { get; set; }

        /// <summary>
        /// Relationship with the Coin
        /// </summary>
        public int CoinId { get; set; }

        /// <summary>
        /// Relationship with the Coin
        /// </summary>
        public Coin Coin { get; set; }
    }
}
